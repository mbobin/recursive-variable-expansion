# frozen_string_literal: true

require 'benchmark/ips'
require 'set'

module ExpandVariables
  class << self
    def expand(value, variables)
      variables_hash = nil

      value.gsub(/\$([a-zA-Z_][a-zA-Z0-9_]*)|\${\g<1>}|%\g<1>%/) do
        variables_hash ||= transform_variables(variables)
        variables_hash[$1 || $2]
      end
    end

    def recursive_expansion(values, variables)
      values.inject(variables.dup) do |acc, var|
        acc.push(key: var[:key], value: expand(var[:value], acc))
      end
    end

    private

    def transform_variables(variables)
      # Lazily initialise variables
      variables = variables.call if variables.is_a?(Proc)

      # Convert hash array to variables
      if variables.is_a?(Array)
        variables = variables.reduce({}) do |hash, variable|
          hash[variable[:key]] = variable[:value]
          hash
        end
      end

      variables
    end
  end
end

module RecursiveExpandVariables
  class << self

    def expand(value, variables)
      variables_hash = nil

      variable_pattern = /\$([a-zA-Z_][a-zA-Z0-9_]*)|\${\g<1>}|%\g<1>%/
      expanded_variables = Set.new
      while value.match(variable_pattern)
        variables_hash ||= transform_variables(variables)
        current_pass_expanding_variables = Set.new
        value = value.gsub(variable_pattern) do
          current_pass_expanding_variables.add($1)
          substitute_variable(expanded_variables, variables_hash, $1)
        end
        expanded_variables.merge(current_pass_expanding_variables)
      end
      value
    end

    private

    def substitute_variable(past_expanded_variables, variables_hash, variable_name)
      value = '' if past_expanded_variables.include?(variable_name)
      value || variables_hash[variable_name]
    end

    def transform_variables(variables)
      # Lazily initialise variables
      variables = variables.call if variables.is_a?(Proc)

      # Convert hash array to variables
      if variables.is_a?(Array)
        variables = variables.reduce({}) do |hash, variable|
          hash[variable[:key]] = variable[:value]
          hash
        end
      end

      variables
    end
  end
end

expansions = ARGV.first.to_i

puts "Running with #{expansions} variables"

yaml_variables = (1..expansions)
  .to_a
  .map { |index| { key: "VAR_#{index}", value: "${VAR_#{index - 1}}_#{index}" } }

initial_variables = [{ key: "VAR_0", value: "0" }]
all_variables = initial_variables + yaml_variables

Benchmark.ips do |x|
  x.config(time: 5, warmup: 2)

  x.report("with recursive regexp") do
    yaml_variables.map do |var|
      value = RecursiveExpandVariables.expand(var[:value], all_variables)
      value = { key: var[:key], value: value }
    end
  end

  x.report("without recursive regexp") do
    ExpandVariables.recursive_expansion(yaml_variables, initial_variables)
  end

  x.compare!
end
